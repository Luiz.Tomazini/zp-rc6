***Settings***

Documentation       Representação das mensagens de erro exibidas ao realizar cadastro de novo cliente

***Variables***
${NAME_ERROR}                xpath://label[@for='name']//span[text()='Nome é obrigatório']
${CPF_ERROR}                 xpath://small[contains(text(),"CPF é obrigatório")]
${ADDRESS_ERROR}             xpath://label[@for='name']//span[text()='Endereço é obrigatório']     
${PHONE_ERROR}               xpath://small[text()="Telefone é obrigatório"]
${CPF_INCORRECT}             xpath://small[text()="CPF inválido"]
${PHONE_INCORRECT}           xpath://small[text()="Telefone inválido"]