***Settings***

Documentation       Representação da página clientes com seus elementos e ações

***Variables***
${CUSTOMERS_FORM}               css:a[href$=register]
${LABEL_NAME}                   css:label[for=name]
${LABEL_CPF}                    css:label[for=cpf]
${LABEL_ADDRESS}                css:label[for=address]
${LABEL_PHONE}                  css:label[for=phone_number]
${SELECT_FORM}                  xpath://td[text()="233.333.333-33"]
${DELETE_FORM}                  xpath://button[text()="APAGAR"]
${RETURN_CUSTOMERS_LIST}        xpath://button[text()="VOLTAR"]


***Keywords***
Register New Customer 
    [Arguments]     ${name}       ${cpf}       ${address}       ${phone_number}
    
    Input text          id:name                  ${name}
    Input text          id:cpf                   ${cpf}
    Input text          id:address               ${address}
    Input text          id:phone_number          ${phone_number}

    Click Element       xpath://button[text()="CADASTRAR"]

Remover cliente
    Wait Until Element Is Visible   ${SELECT_FORM}           5   
    Click Element                   ${SELECT_FORM}
    Wait Until Element Is Visible   ${DELETE_FORM}           5   
    Click Element                   ${DELETE_FORM}