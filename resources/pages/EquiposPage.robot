***Settings***

Documentation       Representação da página equipos com seus elementos e ações

***Variables***
${EQUIPOS_FORM}    css:a[href$=register]
${EQUIPOS_NAME}    css:label[for=equipo-name]
${EQUIPOS_DAILY}   css:label[for=daily_price]


***Keywords***
Register New Equipo
    [Arguments]     ${name}       ${daily}
    
    Input text          id:equipo-name                   ${name}
    Input text          id:daily_price                   ${daily}


    Click Element       xpath://button[text()="CADASTRAR"]



    