
***Keywords***
##Login
Acesso a página Login
    Go To       ${base_url}

Submeto minhas credenciais
    [Arguments]     ${email}    ${password}

    Login With      ${email}    ${password}

Devo ver a área logada
    Wait Until Page Contains      Aluguéis      5
   

Devo ver um toaster com a mensagem
    [Arguments]     ${expect_message}

    Wait Until Element Contains          ${TOASTER_ERROR}     ${expect_message}

## Cadastro Customers
Dado que acesso o formulário de cadastro de clientes
    Go To Customers

    Wait Until Element Is Visible   ${CUSTOMERS_FORM}          5   
    Click Element                   ${CUSTOMERS_FORM}

E que tenho o seguinte cliente:
    [Arguments]     ${name}       ${cpf}       ${address}       ${phone_number}

    Remove Customer By cpf      ${cpf}

    Set Test Variable     ${name}
    Set Test Variable     ${cpf}
    Set Test Variable     ${address}
    Set Test Variable     ${phone_number}      

Quando faço a inclusão desse cliente    
    Register New Customer       ${name}       ${cpf}       ${address}       ${phone_number}


Então devo ver a notificação:
    [Arguments]     ${expect_notice}   

    Wait Until Element Contains       ${TOASTER_SUCCESS}        ${expect_notice}        5  


Então devo ver a notificação campos obrigatórios:
    Wait Until Element Contains   ${LABEL_NAME}     Nome é obrigatório            5
    Wait Until Element Contains   ${LABEL_CPF}      CPF é obrigatório             5
    Wait Until Element Contains   ${LABEL_ADDRESS}  Endereço é obrigatório        5 
    Wait Until Element Contains   ${LABEL_PHONE}    Telefone é obrigatório        5



Então devo ver a notificação de erro:
      [Arguments]     ${output_erro}   

    Wait Until Page Contains            ${output_erro}               5
    Wait Until Page Contains            ${output_erro}               5

Então devo ver o texto:
    [Arguments]       ${expect_message}

    Wait Until Page Contains        ${expect_message}         5


## Exclusão Customers
E insira um cliente indesejado:
    [Arguments]                 ${name}       ${cpf}       ${address}       ${phone_number}
    Register New Customer       ${name}       ${cpf}       ${address}       ${phone_number}

E retorno a lista de clientes
    sleep   12
    Wait Until Element Is Visible   ${RETURN_CUSTOMERS_LIST}           5   
    Click Element                   ${RETURN_CUSTOMERS_LIST}

Quando eu removo este cliente
    Remover Cliente
##Equipos
Dado que acesso o formulário de cadastro de Equipos
    Wait Until Element Is Visible   ${NAV_EQUIPOS}            5   
    Click Element                   ${NAV_EQUIPOS}
    Wait Until Element Is Visible   ${EQUIPOS_FORM}           5   
    Click Element                   ${EQUIPOS_FORM}

E que eu tenho o seguinte equipo
   [Arguments]     ${name}       ${daily}

    Remove Equipo By name      ${name}

    Set Test Variable       ${name}
    Set Test Variable       ${daily}

Mas este equipo já existe no sistema

    Insert Equipo        ${name}       ${daily}

Quando faço a inclusão correta desse Equipo
    [Arguments]     ${name}       ${daily}
    
    Register New Equipo        ${name}       ${daily}

    Remove Equipo By name      ${name}
    

Quando faço a inclusão errada desse Equipo
    [Arguments]     ${name}       ${daily}
    
    Register New Equipo        ${name}       ${daily}

Quando faço a inclusão desse Equipo
    
    Register New Equipo        ${name}       ${daily}

Então devo ver a notificação de erro em nome

    Wait Until Element Contains    ${EQUIPOS_NAME}       Nome do equipo é obrigatório         5

Então devo ver a notificação de erro em Valor diário

    Wait Until Element Contains    ${EQUIPOS_DAILY}       Diária do equipo é obrigatória      5


Então devo ver a notificação de equipo existente  
    [Arguments]     ${expect_message}

    Wait Until Element Contains          ${TOASTER_ERROR}     ${expect_message}        5


##Exclusão de Equipos
Equipo à ser deletado
    [Arguments]     ${name}       ${daily}
    
    Delete New Equipo       ${EQUIPOS_DELETE}       5

