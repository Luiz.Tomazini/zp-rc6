***Settings***
Documentation   Cadastro Tentativa

Resource      ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session

Test Template   Tentativa de cadastro

***Keywords***

Tentativa de cadastro
    [Arguments]     ${name}  ${cpf}  ${address}  ${phone_number}  ${output_erro}   

    Dado que acesso o formulário de cadastro de Clientes
    Quando faço a inclusão desse cliente:   ${name}  ${cpf}  ${address}  ${phone_number}
    Então devo ver a notificação de erro:   ${output_erro}
 
    
***Test Cases***                                      

#Campo nome vazio              ${EMPTY}       99999999999         Rua antonio,50       11999999999     Nome é obrigatório                                            
Campo cpf vazio               Lucas          ${EMPTY}            ${EMPTY}             11999999999     CPF é obrigatório
Campo Endereço vazio          Lucas          99999999999         ${EMPTY}             11999999999     Endereço é obrigatório
          


   