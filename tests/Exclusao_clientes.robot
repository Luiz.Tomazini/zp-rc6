***Settings***
Documentation       Cadastro de Clientes
Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session



***Test Cases***
Remover Cliente
    Dado que acesso o formulário de cadastro de clientes
    E insira um cliente indesejado:
    ...  Bob esponja    23333333333     Rua das palmeiras,756       21999999999
    E retorno a lista de clientes
    Quando eu removo este cliente
    Então devo ver a notificação:  Cliente removido com sucesso!

