***Settings***

Resource        ../resources/base.robot

Documentation       Login

#Executa uma ou mais Keywords apos execucao de todos os steps de cada caso de teste
Test Setup          Start Session

Test Teardown       Finish Session

***Test Cases***
Login do Administrador
    Acesso a página Login
    Submeto minhas credenciais  admin@zepalheta.com.br  pwd123
    Devo ver a área logada

Senha incorreta
    [tags]      inv_login
    Acesso a página Login
    Submeto minhas credenciais      admin@zepalheta.com.br  abc123
    Devo ver um toaster com a mensagem  Ocorreu um erro ao fazer login, cheque as credenciais.

Email incorreto
    [tags]      inv_login
    Acesso a página Login
    Submeto minhas credenciais      admin@zespaleta.com.br  pwd123
    Devo ver um toaster com a mensagem  Ocorreu um erro ao fazer login, cheque as credenciais.

Senha em branco
    [tags]      inv_login
    Acesso a página Login
    Submeto minhas credenciais      admin@zespaleta.com.br  ${EMPTY}
    Devo ver um toaster com a mensagem  O campo senha é obrigatório!

Email em branco
    [tags]      inv_login
    Acesso a página Login
    Submeto minhas credenciais      ${EMPTY}  pwd123
    Devo ver um toaster com a mensagem  O campo email é obrigatório!

Email e senha em branco
    [tags]      inv_login
    Acesso a página Login
    Submeto minhas credenciais       ${EMPTY}    ${EMPTY}
    Devo ver um toaster com a mensagem  Os campos email e senha não foram preenchidos!