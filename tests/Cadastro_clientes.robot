***Settings***
Documentation       Cadastro de Clientes
Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session



***Test Cases***
Novo Cliente
    Dado que acesso o formulário de cadastro de Clientes
    E que tenho o seguinte cliente:
        ...     Bon Jovi        00000001406     Rua dos Bugs, 1000      11999999999
    Quando faço a inclusão desse cliente

    Então devo ver a notificação:       Cliente cadastrado com sucesso!

Campos obrigatórios
        [tags]      temp
    Dado que acesso o formulário de cadastro de Clientes
    E que tenho o seguinte cliente:
        ...     ${EMPTY}       ${EMPTY}     ${EMPTY}      ${EMPTY}
    Quando faço a inclusão desse cliente

    Então devo ver a notificação campos obrigatórios:

Nome é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    ${EMPTY}    63294715099     Rua dos bugs, 1000      11999999999     Nome é obrigatório

CPF é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Luiz    ${EMPTY}     Rua dos bugs, 1000      11999999999     CPF é obrigatório

Endereço é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Luiz    63294715099    ${EMPTY}      11999999999     Endereço é obrigatório

Telefone é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Luiz    63294715099    Rua dos bugs, 1000       ${EMPTY}     Telefone é obrigatório

Telefone é inválido
    [Tags]      required
    [Template]  Validação de Campos
    Luiz    63294715099    Rua dos bugs, 1000       1199999999    Telefone inválido

CPF é inválido
    [Tags]      required
    [Template]  Validação de Campos
    Luiz    6329471509    Rua dos bugs, 1000       11999999999    CPF inválido

 
***Keywords***
Validação de Campos 
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}     ${expect_message}

    Dado que acesso o formulário de cadastro de clientes
    E que tenho o seguinte cliente:
    ...   ${name}     ${cpf}      ${address}      ${phone_number}   
    Quando faço a inclusão desse cliente
    Então devo ver o texto:                       ${expect_message}