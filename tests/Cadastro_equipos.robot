***Settings***
Documentation       Cadastro de Equipos
Resource        ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session


***Test Cases***
Novo Equipo
    Dado que acesso o formulário de cadastro de Equipos
    Quando faço a inclusão correta desse Equipo
    ...     Bateria       1     
    Então devo ver a notificação:       Equipo cadastrado com sucesso!

Nome Obrigatório
    Dado que acesso o formulário de cadastro de Equipos
    Quando faço a inclusão errada desse Equipo
    ...     ${EMPTY}       1     
    Então devo ver a notificação de erro em nome

Valor Diário Obrigatório
    Dado que acesso o formulário de cadastro de Equipos
    Quando faço a inclusão errada desse Equipo
    ...     Bateria   ${EMPTY}          
    Então devo ver a notificação de erro em Valor diário

Equipo Duplicado
    Dado que acesso o formulário de cadastro de Equipos
    E que eu tenho o seguinte equipo
    ...     Guitarra       1
    Mas este equipo já existe no sistema    
    Quando faço a inclusão desse Equipo
    Então devo ver a notificação de equipo existente   Este Equipo já existe no sistema!    